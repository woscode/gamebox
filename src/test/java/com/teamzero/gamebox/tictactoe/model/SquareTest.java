package com.teamzero.gamebox.tictactoe.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.teamzero.gamebox.model.tictactoe.Mark;
import com.teamzero.gamebox.model.tictactoe.Square;
import org.junit.jupiter.api.Test;

class SquareTest {

	@Test
	void init() {
		
		Square square = new Square(0);
		assertTrue(square.isEmpty());
		assertEquals(Mark.EMPTY, square.getMark());
		assertEquals("", square.toString());
		
		square.setMark(Mark.X);
		assertFalse(square.isEmpty());
	}
}
