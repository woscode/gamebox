package com.teamzero.gamebox.repository;

import com.teamzero.gamebox.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long>{
}
