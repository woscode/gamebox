package com.teamzero.gamebox.service;

import com.teamzero.gamebox.model.User;

public interface UserService {

    void save(User user);
    User findByUsername(String username);
    User findByNickname(String nickname);
}
