package com.teamzero.gamebox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GameBoxDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GameBoxDemoApplication.class, args);
	}

}
